/*
Matt Juntunen
A test for my implementation of the RC4 algorithm.
*/

#include <cstdlib>
#include <stdio.h>
#include <cstring>
#include "rc4.h"

char result[200];
char temp[200];
void test(const char * key, const char * input, 
	const char * expected, int inplace){

	RC4 rc;
	
	printf("-----\n");
	printf("Key: %s\n", key);
	printf("Input: %s\n", input);
	
	int len = strlen(input);
	rc.init(key);
	
	if (inplace){
		printf("Processing buffer in place ...\n");
		strcpy(temp, input);
		rc.process((unsigned char *)temp, len);
		
	} else {
		printf("Copying buffer while processing ...\n");
		memset(temp, 0, 200);
		rc.process((unsigned char *)input, (unsigned char *)temp, len);
	}
	
	//convert to hex string
	int i;
	unsigned char * t = (unsigned char *)temp;
	for (i = 0; i < len; i++){
		sprintf(result + (i*2), "%02X", (unsigned char)temp[i]);
	}
	
	printf("Expected: %s\n", expected);
	printf("Result: %s\n", result);
	
	if (strcmp(expected, result) != 0){
		printf("FAILED!! Test results did not match expected results!\n");
		exit(1);
	} else {
		printf("PASS\n");
	}
} 

int main(){

	test("Key", "Plaintext", "BBF316E8D940AF0AD3", 1);
	test("Key", "Plaintext", "BBF316E8D940AF0AD3", 0);
	
	test("Wiki", "pedia", "1021BF0420", 1);
	test("Wiki", "pedia", "1021BF0420", 0);
	
	test("Secret", "Attack at dawn", "45A01F645FC35B383552544B9BF5", 1);
	test("Secret", "Attack at dawn", "45A01F645FC35B383552544B9BF5", 0);
		
	
	//some anecdotal testing	
	printf("\n");
	 	
	RC4 * rc4 = new RC4();
	char key[] = "Secret";
	
	rc4->init(key);
	
	//print out some of the key stream
	for (int i=0; i<20; i++){
		printf("%02x", rc4->next());
	}
	printf("\n");
	
	//re-initialize the obj
	rc4->init(key);
	
	const char * plain = "Attack at dawn";
	
	int cipher[14];
	
	for (int i=0; i<strlen(plain); i++){
		cipher[i] = rc4->process(plain[i]);
		printf("%02x", cipher[i]);
	}
	printf("\n");

	//decipher
	
	//re-initialize the obj
	rc4->init(key);
	
	for (int i=0; i<strlen(plain); i++){
		printf("%c", rc4->process(cipher[i]));
	}
	printf("\n");

	delete rc4;

}
