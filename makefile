files = rc4.h rc4.cpp crc32.h crc32.cpp wep.h wep.cpp byteutils.h byteutils.cpp \
	cmdutils.h cmdutils.cpp

wepexploit : $(files) wepexploit.cpp
	g++ -pedantic-errors $(files) wepexploit.cpp -o wepexploit
	
runtests: rc4test crctest weptest
	./crctest
	./rc4test
	./weptest	
	
rc4test: $(files) rc4test.cpp
	g++ -pedantic-errors $(files) rc4test.cpp -o rc4test
	
crctest : $(files) crctest.cpp
	g++ -pedantic-errors $(files) crctest.cpp -o crctest 	

weptest : $(files) weptest.cpp
	g++ -pedantic-errors $(files) weptest.cpp -o weptest
	
