/*
Matt Juntunen
4/11/13
Implementation of the CRC32 algorithm,
taken primarily from the sample code at http://www.ietf.org/rfc/rfc1952.txt.
*/

#include "crc32.h"

/* public members */
CRC32::CRC32(){
	current_crc = 0L;
}

unsigned long CRC32::get_crc(){
	return current_crc;
}

unsigned long CRC32::process(unsigned char * buf, int len){
	current_crc = update_crc(current_crc, buf, len);
	return current_crc;
}

unsigned long CRC32::update_crc(
	unsigned long crc, unsigned char * buf, int len){
	unsigned long c = crc ^ 0xffffffffL;
	int n;
	
	if (!table_made){
		make_table();
	}
	
	for (n = 0; n < len; n++){
		c = crc_table[(c ^ buf[n]) & 0xff] ^ (c >> 8);
	}
	 return c ^ 0xffffffffL;
}

unsigned long CRC32::crc(unsigned char * buf, int len){
	return update_crc(0L, buf, len);
}

/* private members */

unsigned long CRC32::crc_table[ CRC32_TABLE_LEN ];

int CRC32::table_made = 0;

void CRC32::make_table(){
	unsigned long c;
	int n,k;
	
	for (n = 0; n < CRC32_TABLE_LEN; n++){
		c = (unsigned long) n;
		for (k = 0; k < 8; k++){
			if (c & 1){
				c = 0xedb88320L ^ (c >> 1);
			} else {
				c = c >> 1;
			}
		}
		crc_table[n] = c;
	}
	table_made = 1;
}
