#!/usr/bin/perl
my ($file_in, $file_out) = @ARGV;
my ($fin, $fout);

open ($fin, "<", $file_in) or die "couldn't open file $file_in";
open ($fout, ">", $file_out) or die "couldn't open file $file_out";

my $line;
while ($line = <$fin>){
	$line =~ s/\b([0-9A-Fa-f]{2})/0x$1,/g;
	print $fout $line;
}
close $fin;
close $fout;
