/*
Matt Juntunen
4/11/13
A collection of utility methods for working with bytes.
*/

#ifndef __BYTEUTILS_H__
#define __BYTEUTILS_H__

//gets the octet at the given position (1-based) from the right of the number
unsigned char octet(long n, int pos);

//prints the byte hex values found at buf
void print_hex(void * buf, int buf_len, int bytes_per_line);

#endif
