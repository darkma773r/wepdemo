/*
Matt Juntunen
4/11/13
An implementation of the RC4 algorithm.
*/

#ifndef __RC4_H__
#define __RC4_H__

const int s_length = 256;

class RC4 {
	public:
		RC4();
		~RC4();
		
		//initializes the object with the given key
		void init(unsigned char * key, int key_len); 
			
		//initializes the object the actual byte values of the given key.
		//key must be a null-terminated string.
		void init(const char * key);
		
		//gets the next byte from the keystream	
		unsigned char next(); 
		
		//gets the next byte from the keystream and XOR's it with val
		unsigned char process(unsigned char val); 
			
		//process all of buf, placing the output in outbuf	
		void process(void * buf, void * outbuf, int len);	
			
		//process buf in place		
		void process(void * buf, int len);
			
	private:
		int i;
		int j;
		unsigned char * S; //points to the permutation array	
		int keylength;
};

#endif
