/*
Matt Juntunen
4/11/13
A class for working with command-line arguments.
*/

#ifndef __CMDUTILS_H__
#define __CMDUTILS_H__

class CmdUtils {
	private:
		int argc;
		const char ** argv;
		
	public:
		CmdUtils(int argc, const char ** argv);
		
		//gets the number of commandline args (does not include the exe name)
		int count();
		
		//gets the name of the executable given on the commandline
		const char * exe_name();
		
		//returns 1 if the command line arguments contain the given argument
		//or 0 if not
		int has_arg(const char * arg, int case_sensitive = 0);
		
		//gets the argument after the given flag; for example if the command 
		//line is " -f hello" arg_for_name("-f") will return "hello"
		const char * arg_for_name(const char * flag, int case_sensitive = 0); 
};

#endif
