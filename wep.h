/*
Matt Juntunen
4/11/13
An implementation of the WEP protocol. Includes the WepBuffer class for holding
and working with WEP data and the Wep class for creating and reading WEP
packets.
*/

#ifndef __WEP_H__
#define __WEP_H__

#include "rc4.h"
#include "crc32.h"

//a class for wrapping WEP packet functionality around a data buffer.
class WepBuffer {
	private:
		unsigned char * buf;
		int len;
		int packet_len;
	
	public:
		WepBuffer(int size);	
		~WepBuffer();
		
		//returns a pointer to the iv portion of the packet
		unsigned char * iv();
		
		//returns a pointer to the key id portion of the packet
		unsigned char * key_id();
		
		//returns a pointer to the data portion of the packet
		unsigned char * data();
		
		//returns a pointer to the icv portion of the packet
		unsigned char * icv();
		
		//writes the byte to the buffer at the current write pointer and
		//increments the pointer. Provides an easy way to fill the buffer
		//and keep track of the packet size at the same time.
		void write(unsigned char byte);
		
		//moves the write pointer to the beginning of the buffer
		void clear();
		
		//returns the size of the current packet
		int packet_length();
		
		//returns the size of the data section (packet length minus WEP 
		//overhead)
		int data_length();
		
		//returns the total size of the buffer
		int buffer_length();
		
		//returns a pointer to the start of the internal buffer
		unsigned char * buffer();
		
		//prints the WEP packet to the screen in a friendly format
		void print();
};

//a class for creating and reading WEP packets
class Wep {
	private:
		RC4 * rc4;
		
	public:
		Wep();
		~Wep();
		
		//creates a WEP packet in "out" with the data from "in" using
		//the given key (key_ptr / key_len). The iv for the packet is 
		//generated randomly.
		void create(WepBuffer * out, void * in, int data_len,
			unsigned char * key_ptr, int key_len);	
		
		//creates a WEP packet in "out" with the data from "in" using
		//the given key (key_ptr / key_len) and the given iv.
		void create(WepBuffer * out, void * in, int data_len,
			unsigned char * key_ptr, int key_len, long iv);	
		
		//decrypts the packet from "in" and places in "outbuf" using the
		//given key (key_ptr / key_len). Returns the number of bytes written to
		//"outbuf"
		int read(void * outbuf, int outbuf_len, WepBuffer * in,
			unsigned char * key_prt, int key_len);	
	
		//some constants for working with WEP data	
		static const int MIN_WEP_SIZE = 9;
		static const int WEP_OVERHEAD = 8;
		
		static const int IV_OFFSET = 0;
		static const int IV_LEN = 3;
		
		static const int KEY_ID_OFFSET = 3;
		static const int KEY_ID_LEN = 1;
		
		static const int DATA_OFFSET = 4;
		
		static const int ICV_LEN = 4;		 
};

#endif
