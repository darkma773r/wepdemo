/*
Matt Juntunen
4/11/13
A class for working with command-line arguments.
*/

#include <string.h>
#include <ctype.h>
#include "cmdutils.h"

//custom strcmp that can be case sensitive or not
int strcmp_custom(const char * str1, const char * str2, int case_sensitive){
	char ch1, ch2;
	int idx = 0;
	do {
		if (case_sensitive){
			ch1 = str1[idx];
			ch2 = str2[idx];
		} else { //not case sensitive
			ch1 = toupper(str1[idx]);
			ch2 = toupper(str2[idx]);
		}
		
		if (ch1 != ch2){
			return ch1 - ch2;
		}
	
		idx++;
	} while (ch1 != '\0' && ch2 != '\0');

	return ch1 - ch2;	
}

CmdUtils::CmdUtils(int count, const char ** vector): 
	argc(count), argv(vector)
{
}

int CmdUtils::count(){
	return argc - 1; //the first argument should be the executable name, 
		//so skip it
}

const char * CmdUtils::exe_name(){
	if (argc > 0){
		return argv[0];
	}
	return NULL;
}

int CmdUtils::has_arg(const char * arg, int case_sensitive){
	int i;
	for (i=1; i<argc; i++){
		if (strcmp_custom(argv[i], arg, case_sensitive) == 0){
			return 1;
		}
	}
	return 0;
}

const char * CmdUtils::arg_for_name(const char * flag, int case_sensitive){
	int i;
	for (i=1; i<argc - 1; i++){ //less than argc - 1 since we're looking for
							//the argument AFTER the match
		if (strcmp_custom(argv[i], flag, case_sensitive) == 0){
			return argv[i+1];
		}
	}
	
	return NULL;
}
