/*
Matt Juntunen
A test for my implementation of CRC32.
*/

#include <cstdlib>
#include <stdio.h>
#include <cstring>
#include "crc32.h"

void matches(const char * input, unsigned long expected, unsigned long test){
	printf("-----\n");
	printf("Input: %s\n", input);
	printf("Expected: %x\n", expected);
	printf("Test: %x\n", test);
	
	if (expected != test){
		printf("FAIL!! Test value does not match expected value\n");
		exit(1);
	} else {
		printf("Pass\n");
	}
}

int main(){
	CRC32 * c = new CRC32();
	
	const char * v1 = "The quick brown fox jumps over the lazy dog";
	c->process((unsigned char *)v1, strlen(v1));
	matches(v1, 0x414fa339L, c->get_crc());
	
	delete c;
	
	
	c = new CRC32();
	
	const char * v2a = "Test vector fro";
	const char * v2b = "m febooti.com";
	c->process((unsigned char *)v2a, strlen(v2a));
	c->process((unsigned char *)v2b, strlen(v2b));
	c->get_crc();
	
	char * v2 = new char[strlen(v2a) + strlen(v2b) + 1];
	strcpy(v2, v2a);
	strcat(v2, v2b);
	matches(v2, 0x0c877f61, c->get_crc());

	delete [] v2;
	delete c;	
	
	c = new CRC32();
	
	const char * v3 = "";
	c->process((unsigned char *)v3, strlen(v3));
	matches(v1, 0x0L, c->get_crc());
	
	delete c;
}
