/*
Matt Juntunen
4/11/13
Implementation of the CRC32 algorithm,
taken primarily from the sample code at http://www.ietf.org/rfc/rfc1952.txt.
*/

#ifndef __CRC32_H__
#define __CRC32_H__

#define CRC32_TABLE_LEN 256

class CRC32 {
	public:
		CRC32();
		
		/* return the object's current crc value */
		unsigned long get_crc();
		
		/* processes the buffer and adds it to the current crc */
		unsigned long process(unsigned char * buf, int len);
		
		/* calculates a single crc for the given buffer with the given 
		starting crc*/
		static unsigned long update_crc(
			unsigned long crc, unsigned char * buf, int len);
		
		/* calculates the crc for the given buffer */	
		static unsigned long crc(
			unsigned char * buf, int len);
		
	private:
		/* the lookup table shared by all objects */
		static unsigned long crc_table[ CRC32_TABLE_LEN ];
	
		/* function to actually create the shared table */
		static void make_table();
		
		/* flag to determine if the crc table has been made yet */
		static int table_made;
		
		/* holds current crc for the object */
		unsigned long current_crc;
};

#endif
