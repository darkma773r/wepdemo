/*
Matt Juntunen
4/11/13
A collection of utility methods for working with bytes.
*/

#include <stdio.h>
#include "byteutils.h"

unsigned char octet(long n, int pos){
	return (unsigned char)((n >> ((pos-1)*8)) & 0xff);
}

void print_hex(void * buf, int buf_len, int bytes_per_line){
	unsigned char * b = (unsigned char *)buf;
	
	int counter = 0;
	int i;
	for (i=0; i<buf_len; i++){
		printf("%02x", b[i]);
		
		counter++;
		if (counter < bytes_per_line){
			printf(" ");
		} else {
			printf("\n");
			counter = 0;
		}
	}
}
