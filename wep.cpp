/*
Matt Juntunen
4/11/13
An implementation of the WEP protocol. Includes the WepBuffer class for holding
and working with WEP data and the Wep class for creating and reading WEP
packets.
*/

#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdexcept>
#include "wep.h"
#include "byteutils.h"

/******** WepBuffer ******/

WepBuffer::WepBuffer(int size):
	buf(NULL), len(size), packet_len(0)
{
	if (size < Wep::MIN_WEP_SIZE){
		throw std::runtime_error("WepBuffer size is too small");
	}
	buf = new unsigned char[size];
}

WepBuffer::~WepBuffer(){
	if (buf != NULL){
		delete [] buf;
	}
}

unsigned char * WepBuffer::iv(){
	return buf;
}

unsigned char * WepBuffer::key_id(){
	return buf + Wep::KEY_ID_OFFSET;
}

unsigned char * WepBuffer::data(){
	return buf + Wep::DATA_OFFSET;
}

unsigned char * WepBuffer::icv(){
	return buf + (packet_len - Wep::ICV_LEN);
}

void WepBuffer::write(unsigned char byte){
	if (packet_len >= len){
		throw std::runtime_error("Cannot write byte: buffer is full.");
	}
	*(buf + packet_len++) = byte;
}

void WepBuffer::clear(){
	packet_len = 0;
}

int WepBuffer::packet_length(){
	return packet_len;
}

int WepBuffer::data_length(){
	return packet_len - Wep::WEP_OVERHEAD;
}

int WepBuffer::buffer_length(){
	return len;
}

unsigned char * WepBuffer::buffer(){
	return buf;
}

void WepBuffer::print(){
	printf("[packet length: %d]\n", packet_len);

	printf("[iv : %d]\n", Wep::IV_LEN);
	print_hex(iv(), Wep::IV_LEN, 16);
	printf("\n");
	
	printf("[keyid : %d]\n", Wep::KEY_ID_LEN);
	print_hex(key_id(), Wep::KEY_ID_LEN, 16);
	printf("\n");
	
	printf("[data : %d]\n", data_length());
	print_hex(data(), data_length(), 16);
	printf("\n");
	
	printf("[icv : %d]\n", Wep::ICV_LEN);
	print_hex(icv(), Wep::ICV_LEN, 16);
	printf("\n");
}



/******** Wep ******/
Wep::Wep(){
	rc4 = new RC4();
}

Wep::~Wep(){
	delete rc4;
}

void Wep::create(WepBuffer * out, void * inbuf, int data_len, 
	unsigned char * key_ptr, int key_len){
	this->create(out, inbuf, data_len, key_ptr, key_len, rand());
}

void Wep::create(WepBuffer * out, void * inbuf, int data_len, 
	unsigned char * key_ptr, int key_len, long iv){
		
	unsigned char * in = (unsigned char *)inbuf;

	int i; //general loop index

	if (out->buffer_length() < data_len + WEP_OVERHEAD){
		throw std::runtime_error("WepBuffer is too small for the given data");
	} 

	//create the full key, including the iv, in a temporary buffer
	unsigned char * full_key = new unsigned char[key_len + IV_LEN];

	for (i=0; i<IV_LEN; i++){
		full_key[i] = octet(iv, IV_LEN - i);
	}
	
	memcpy(full_key + IV_LEN, key_ptr, key_len);
	
	//initialize the rc4 generator with the combined key
	rc4->init(full_key, key_len + IV_LEN);
	
	//we no longer need the key so remove it
	delete [] full_key;
	
	//move the WepBuffer's internal pointer to the beginning
	out->clear(); 
	
	//write the iv to the packet buffer
	for (i=0; i<IV_LEN; i++){
		out->write(octet(iv, IV_LEN - i));
	}
	
	//write the key_id byte; use 0 for now since it doesn't really matter to us
	out->write(0);
	
	//calculate the icv value
	unsigned long icv = CRC32::crc(in, data_len);
	
	//write the encrypted data to the output
	for (i=0; i<data_len; i++){
		out->write(rc4->process(in[i]));
	}
	
	//write the encrypted icv to the output, breaking it into 4 bytes
	for (i=0; i<ICV_LEN; i++){
		out->write(rc4->process(octet(icv, i+1))); 
			//IMPORTANT!! - the icv goes in lowest byte first; everything else
			//is going in highest byte first
	}
	//done!!
}

int Wep::read(void * outbuf, int outbuf_len, WepBuffer * in,
			unsigned char * key_ptr, int key_len){
	unsigned char * out = (unsigned char *)outbuf;
	int i; //general loop index
	
	if (in->packet_length() < MIN_WEP_SIZE){
		//not enough data in the WEP buffer for a packet. We can't really
		//do anything with this so return 0;
		return 0;
	}
	
	if (outbuf_len < in->packet_length() - WEP_OVERHEAD){
		throw std::runtime_error("Output buffer is not large enough for WEP data");
	}
	
	//create the full key in a temporary buffer, pulling the iv from the
	//WEP packet
	unsigned char * full_key = new unsigned char[key_len + IV_LEN];
	unsigned char * ivbuf = in->iv(); //get a reference to the iv section
	
	memcpy(full_key, ivbuf, IV_LEN); //copy the iv
	memcpy(full_key + IV_LEN, key_ptr, key_len); //copy the key
	
	//initialize the rc4 stream
	rc4->init(full_key, key_len + IV_LEN);
	
	//we no longer need the key
	delete [] full_key;
	
	//NOTE: we don't need to worry about the key id in this implementation
	
	//decrypt the data, excluding the icv
	int data_len = in->data_length();
	unsigned char * dbuf = in->data();
	
	for (i=0; i<data_len; i++){
		out[i] = rc4->process(dbuf[i]);
	}
	
	//decrypt the icv into a separate array
	unsigned char * raw_icv = in->icv();
	unsigned char msg_icv[ICV_LEN];
	for (i=0; i<ICV_LEN; i++){
		msg_icv[i] = rc4->process(raw_icv[i]);
	}
	
	//compute the checksum and check it against the decrypted icv
	long computed_crc = CRC32::crc(out, data_len);
	long msg_crc = 0;
	for (i=0; i<ICV_LEN; i++){
		msg_crc |= ( msg_icv[i] << (8*i)); //lowest bytes come first
	}
	
	if (computed_crc != msg_crc){
		throw std::runtime_error("Bad checksum: WEP data is invalid.");
	}
	
	//return number of bytes of actual data read
	return data_len;
}
