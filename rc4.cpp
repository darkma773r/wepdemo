/*
Matt Juntunen
4/11/13
An implementation of the RC4 algorithm.
*/

#include <string.h>
#include "rc4.h"

RC4::RC4()
{
	i = 0; 
	j = 0;
	S = new unsigned char[s_length];
}

RC4::~RC4()
{
	delete [] S;
}

void RC4::init(const char * key)
{
	int len = strlen(key);
	init((unsigned char *)key, len);
}

void RC4::init(unsigned char * key, int key_len)
{
	i = 0;
	j = 0;
	keylength = key_len;
	
	for ( ; i< s_length; i++){
		S[i] = i;
	}

	unsigned char temp;
	for (i = 0; i< s_length; i++){
		j = (j + S[i] + key[i % keylength]) % s_length;
		
		//swap S[i] and S[j]
		temp = S[i];
		S[i] = S[j];
		S[j] = temp;
	} 
	
	//reset for the keystream
	i = 0;
	j = 0;
}

unsigned char RC4::next(){
	i = (i + 1) % s_length;
	j = (j + S[i]) % s_length;
	
	//swap S[i] and S[j]
	unsigned char temp = S[i];
	S[i] = S[j];
	S[j] = temp;
	
	int idx = (S[i] + S[j]) % s_length;
	
	return S[idx];
}

unsigned char RC4::process(unsigned char val){
	return val ^ next();
}

void RC4::process(void * buf, void * outbuf, int len){
	unsigned char * in = (unsigned char *)buf;
	unsigned char * out = (unsigned char *)outbuf;
	
	int i;
	for (i = 0; i < len; i++){
		out[i] = process(in[i]);
	}
}

void RC4::process(void * buf, int len){
	unsigned char * b = (unsigned char *)buf;

	int i;
	for (i = 0; i < len; i++){
		b[i] = process(b[i]);
	}
}
